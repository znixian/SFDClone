/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package mapeditor;

import TWLSlick.TWLStateBasedGame;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;

/**
 *
 * @author Campbell Suter
 */
public class MapEditor extends TWLStateBasedGame {

    private static final Logger LOG = Logger.getLogger(MapEditor.class.getName());

    protected MapEditor(String name) {
        super(name);
    }

    @Override
    protected URL getThemeURL() {
        return getClass().getResource("/mapeditor/assets/simple.xml");
    }

    @Override
    public void initStatesList(GameContainer container) throws SlickException {
        addState(new MapEditorMainState(this));
    }

    //
    // main
    public static void main(String[] args) {
        try {
            AppGameContainer appgc;
            appgc = new AppGameContainer(new MapEditor("SFD Clone - Map Editor"));
//            appgc.setDisplayMode(640, 480, false);
            appgc.setDisplayMode(appgc.getScreenWidth(), appgc.getScreenHeight(), true);
            appgc.setTargetFrameRate(80);
            appgc.start();
        } catch (SlickException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }
}
