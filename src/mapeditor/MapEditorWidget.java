/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package mapeditor;

import de.matthiasmann.twl.Event;
import de.matthiasmann.twl.GUI;
import de.matthiasmann.twl.Widget;
import de.matthiasmann.twl.renderer.lwjgl.LWJGLRenderer;
import java.util.ArrayList;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import sfdclone.Vec2I;
import sfdclone.object.WObject;

/**
 *
 * @author Campbell Suter
 */
public class MapEditorWidget extends Widget {

    private final MapEditorMainState parent;
    private final ArrayList<EditorObject> objs;
    private EditorObject selected;
    private final Vec2I dragOffset = new Vec2I();
    private final Vec2I camPos = new Vec2I();
    private float zoom = 1;
    private boolean dragging;
    private final int gridSize = WObject.SIZE;

    public MapEditorWidget(MapEditorMainState parent) {
        this.parent = parent;
        objs = new ArrayList<>();
    }

    @Override
    protected void paintWidget(GUI gui) {
        LWJGLRenderer renderer = (LWJGLRenderer) gui.getRenderer();
        renderer.pauseRendering();

        GameContainer container = parent.getParent().getContainer();
        Graphics g = container.getGraphics();
        g.setColor(Color.yellow);
        g.fillRect(getX(), getY(), getWidth(), getHeight());

        g.pushTransform();
        g.translate(getX(), getY());
        g.scale(zoom, zoom);
        objs.stream().forEach((obj) -> {
            obj.render(container, g, this);
        });
        if (selected != null) {
            selected.renderOutline(container, g, this);
        }
        g.popTransform();

        renderer.resumeRendering();
    }

    // camera movement
    // drag and drop
    @Override
    protected boolean handleEvent(Event evt) {
        final int x = (int) ((evt.getMouseX() - getX()) / zoom);
        final int y = (int) ((evt.getMouseY() - getY()) / zoom);
        switch (evt.getType()) {
            case MOUSE_WHEEL: {
                zoom += evt.getMouseWheelDelta() / 120f;
                if (zoom < 0.1) {
                    zoom = 0.1f;
                }
                if (zoom > 10) {
                    zoom = 10;
                }
                break;
            }
            case MOUSE_BTNDOWN: {
                int button = evt.getMouseButton();

                if (button != Event.MOUSE_LBUTTON && button != Event.MOUSE_RBUTTON) {
                    break;
                }
                dragging = true;
                selected = null;
                objs.stream().filter((obj) -> (obj.isOver(x, y))).forEach((obj) -> {
                    selected = obj;
                    dragOffset.x = x - obj.getPosition().x;
                    dragOffset.y = y - obj.getPosition().y;
                });
                break;
            }
            case MOUSE_BTNUP: {
                if (evt.getMouseButton() == Event.MOUSE_LBUTTON
                        || evt.getMouseButton() == Event.MOUSE_RBUTTON) {
                    dragging = false;
                }
                break;
            }
            case MOUSE_DRAGGED:
            case MOUSE_MOVED: {
                if (selected == null || !dragging) {
                    break;
                }

                Vec2I pos = selected.getPosition();
                Image image = selected.getType().getImage();

                if ((evt.getModifiers() & Event.MODIFIER_RBUTTON) != 0) {
                    if ((evt.getModifiers() & Event.MODIFIER_LSHIFT) == 0) {
                        int invDragX = selected.getPixelWidth() - dragOffset.x;
                        int invDragY = selected.getPixelHeight() - dragOffset.y;

                        Vec2I size = selected.getSize();

                        int oldSizeX = size.x;
                        int oldSizeY = size.y;

                        size.x = x + invDragX - pos.x;
                        size.y = y + invDragY - pos.y;
//                        System.out.println("ok: " + size);

//                        snapToGrid(size, true);
                        size.x /= image.getWidth();
                        size.y /= image.getHeight();

                        dragOffset.x += (size.x - oldSizeX) * image.getWidth();
                        dragOffset.y += (size.y - oldSizeY) * image.getHeight();

                        selected.setSize(size);
                        break;
                    }
                }

                int prevX = pos.x;
                int prevY = pos.y;

                pos.x = x - dragOffset.x;
                pos.y = y - dragOffset.y;

                snapToGrid(pos, true);

                if (pos.x < 0) {
                    pos.x = 0;
                }
                if (pos.y < 0) {
                    pos.y = 0;
                }
                int maxx = getWidth() - image.getWidth();
                int maxy = getHeight() - image.getHeight();
                if (pos.x > maxx) {
                    pos.x = maxx;
                }
                if (pos.y > maxy) {
                    pos.y = maxy;
                }

                if ((evt.getModifiers() & Event.MODIFIER_RBUTTON) != 0) {
                    if ((evt.getModifiers() & Event.MODIFIER_LSHIFT) != 0) {
                        int widthDiff = (pos.x - prevX) / gridSize;
                        int heightDiff = (pos.y - prevY) / gridSize;
                        selected.getSize().x -= widthDiff;
                        selected.getSize().y -= heightDiff;
                        selected.setSize(selected.getSize());
                        break;
                    }
                    break;
                }
                break;
            }
        }
        return true;
    }

    public void snapToGrid(Vec2I pos, boolean offset) {
        if (offset) {
            pos.x = Math.round(1f * pos.x / gridSize) * gridSize;
            pos.y = Math.round(1f * pos.y / gridSize) * gridSize;
        } else {
            pos.x = (int) (Math.floor(1f * pos.x / gridSize) * gridSize);
            pos.y = (int) (Math.floor(1f * pos.y / gridSize) * gridSize);
        }
    }

    public ArrayList<EditorObject> getObjs() {
        return objs;
    }

    public EditorObject getSelected() {
        return selected;
    }

    public void setSelected(EditorObject selected) {
        this.selected = selected;
    }

    public float getZoom() {
        return zoom;
    }

    public void setZoom(float zoom) {
        this.zoom = zoom;
    }
}
