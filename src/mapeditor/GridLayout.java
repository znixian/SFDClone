/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package mapeditor;

import de.matthiasmann.twl.Widget;

/**
 *
 * @author Campbell Suter
 */
public class GridLayout extends Widget {

    @Override
    protected void layout() {
        if (getNumChildren() == 0) {
            return;
        }
        int x = getInnerX();
        int y = getInnerY();

        int maxChildWidth = 1;
        int maxChildHeight = 1;
        for (int i = 0; i < getNumChildren(); i++) {
            Widget child = getChild(i);
            maxChildWidth = Math.max(maxChildWidth, child.getPreferredWidth());
            maxChildHeight = Math.max(maxChildHeight, child.getPreferredHeight());
        }

        int width = getInnerWidth() / maxChildWidth;

        int margin = 2;
        int cwidth = maxChildWidth + margin * 2;
        int cheight = maxChildHeight + margin * 2;
        for (int i = 0; i < getNumChildren(); i++) {
            Widget child = getChild(i);
            int cx = (i % width) * cwidth + x;
            int cy = i / width * cheight + y;
            child.setPosition(cx + margin, cy + margin);
            child.setSize(maxChildWidth, maxChildHeight);
        }
    }

}
