/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package mapeditor;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;
import sfdclone.Vec2I;

/**
 * Represents a single object in the editor.
 *
 * @author Campbell Suter
 */
public class EditorObject {

    private final EditorObjectType type;
    private Vec2I position;
    private Vec2I size;

    public EditorObject(EditorObjectType type) {
        this.type = type;
        setPosition(null);
        setSize(null);
    }

    public void render(GameContainer container, Graphics g, MapEditorWidget mapEditor) {
        Image image = type.getImage();
        for (int x = 0; x < size.x; x++) {
            for (int y = 0; y < size.y; y++) {
                g.drawImage(image,
                        position.x + x * image.getWidth(),
                        position.y + y * image.getHeight());
            }
        }
    }

    public void renderOutline(GameContainer container, Graphics g, MapEditorWidget mapEditor) {
        Vec2I pos = getPosition();
        Image image = getType().getImage();
        int width = image.getWidth() * size.x;
        int height = image.getHeight() * size.y;

        g.setColor(Color.black);
        g.drawRect(pos.x - 2, pos.y - 2, width + 3, height + 3);

        g.setColor(Color.white);
        g.drawRect(pos.x - 1, pos.y - 1, width + 1, height + 1);
    }

    public boolean isOver(float px, float py) {
        int x = position.x;
        int y = position.y;
        Image image = type.getImage();
        int w = image.getWidth() * size.x;
        int h = image.getWidth() * size.y;
        return px >= x && py >= y
                && px < x + w && py < y + h;
    }

    public EditorObjectType getType() {
        return type;
    }

    public Vec2I getPosition() {
        return position;
    }

    public void getEndPosition(Vec2I toSet) {
        Image image = type.getImage();
        toSet.x = position.x + image.getWidth() * size.x;
        toSet.y = position.y + image.getHeight() * size.y;
    }

    public Vec2I getEndPosition() {
        Vec2I pos = new Vec2I();
        getEndPosition(pos);
        return pos;
    }

    public void setPosition(Vec2I position) {
        this.position = position;
        if (position == null) {
            this.position = new Vec2I();
        }
    }

    public Vec2I getSize() {
        return size;
    }

    public void setSize(Vec2I size) {
        this.size = size;
        if (size == null) {
            size = this.size = new Vec2I();
        }
        if (size.x < 1) {
            size.x = 1;
        }
        if (size.y < 1) {
            size.y = 1;
        }
    }
    
    public int getPixelWidth() {
        return size.x * type.getImage().getWidth();
    }
    
    public int getPixelHeight() {
        return size.y * type.getImage().getHeight();
    }
}
