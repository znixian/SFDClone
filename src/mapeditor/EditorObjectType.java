/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package mapeditor;

import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;

/**
 * Represents a type of object (eg, a crate) in the editor
 *
 * @author Campbell Suter
 */
public class EditorObjectType {

    private final Image image;

    public EditorObjectType(Image image) {
        this.image = image;
    }

    public Image getImage() {
        return image;
    }

    public boolean isResizable() {
        return true;
    }
}
