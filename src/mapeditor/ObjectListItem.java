/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package mapeditor;

import de.matthiasmann.twl.AnimationState;
import de.matthiasmann.twl.Event;
import de.matthiasmann.twl.GUI;
import de.matthiasmann.twl.ThemeInfo;
import de.matthiasmann.twl.Widget;
import de.matthiasmann.twl.renderer.AnimationState.StateKey;
import de.matthiasmann.twl.renderer.lwjgl.LWJGLRenderer;
import org.newdawn.slick.Image;
import sfdclone.Vec2I;
import sfdclone.object.WObject;

/**
 *
 * @author Campbell Suter
 */
public class ObjectListItem extends Widget {

    public static final StateKey STATE_DRAG_ACTIVE = StateKey.get("dragActive");
    public static final StateKey STATE_DROP_OK = StateKey.get("dropOk");
    public static final StateKey STATE_DROP_BLOCKED = StateKey.get("dropBlocked");

    public interface DragListener {

        public void dragStarted(ObjectListItem slot, Event evt);

        public void dragging(ObjectListItem slot, Event evt);

        public void dragStopped(ObjectListItem slot, Event evt);
    }

    private DragListener listener;
    private boolean dragActive;
    private EditorObjectType type;
    private final MapEditorMainState state;
    private final Vec2I temp = new Vec2I();

    public ObjectListItem(MapEditorMainState state) {
        this.state = state;
        int size = WObject.SIZE;
        setMinSize(size, size);
        setMaxSize(size, size);
    }

    public ObjectListItem(MapEditorMainState state, EditorObjectType type) {
        this(state);
        this.type = type;
    }

    public EditorObjectType getType() {
        return type;
    }

    public void setType(EditorObjectType type) {
        this.type = type;
    }

    public boolean canDrop() {
        return true;
    }

    public DragListener getListener() {
        return listener;
    }

    public void setListener(DragListener listener) {
        this.listener = listener;
    }

    public void setDropState(boolean drop, boolean ok) {
        AnimationState as = getAnimationState();
        as.setAnimationState(STATE_DROP_OK, drop && ok);
        as.setAnimationState(STATE_DROP_BLOCKED, drop && !ok);
    }

    @Override
    protected boolean handleEvent(Event evt) {
        if (evt.isMouseEventNoWheel()) {
            if (dragActive) {
                if (evt.isMouseDragEnd()) {
                    if (listener != null) {
                        listener.dragStopped(this, evt);
                    }
                    dragActive = false;
                    getAnimationState().setAnimationState(STATE_DRAG_ACTIVE, false);
                } else if (listener != null) {
                    listener.dragging(this, evt);
                }
            } else if (evt.isMouseDragEvent()) {
                dragActive = true;
                getAnimationState().setAnimationState(STATE_DRAG_ACTIVE, true);
                if (listener != null) {
                    listener.dragStarted(this, evt);
                }
            }
            return true;
        }

        return super.handleEvent(evt);
    }

    @Override
    protected void paintWidget(GUI gui) {
        if (!dragActive && type != null) {
            Image icon = type.getImage();
            LWJGLRenderer renderer = (LWJGLRenderer) gui.getRenderer();
            renderer.pauseRendering();
            icon.draw(getInnerX(), getInnerY(), getInnerWidth(), getInnerHeight());
            renderer.resumeRendering();
        }
    }

    @Override
    protected void paintDragOverlay(GUI gui, int mouseX, int mouseY, int modifier) {
        if (type != null) {
            Image icon = type.getImage();
            LWJGLRenderer renderer = (LWJGLRenderer) gui.getRenderer();
            renderer.pauseRendering();
            final int innerWidth = getInnerWidth();
            final int innerHeight = getInnerHeight();
            float sizeMult = 1;
            temp.x = mouseX - innerWidth / 2;
            temp.y = mouseY - innerHeight / 2;
            MapEditorWidget mapEditor = state.getMapEditor();
            if (mapEditor.isInside(mouseX, mouseY)) {
                temp.x -= mapEditor.getX();
                temp.y -= mapEditor.getY();
                sizeMult = state.getMapEditor().getZoom();
                temp.divide(mapEditor.getZoom());
                mapEditor.snapToGrid(temp, true);
                temp.mult(mapEditor.getZoom());
                temp.x += mapEditor.getX();
                temp.y += mapEditor.getY();
            }
            icon.draw(
                    temp.x,
                    temp.y,
                    innerWidth * sizeMult,
                    innerHeight * sizeMult);
            renderer.resumeRendering();
        }
    }

    @Override
    protected void applyThemeMaxSize(ThemeInfo themeInfo) {
    }

    @Override
    protected void applyThemeMinSize(ThemeInfo themeInfo) {
    }
}
