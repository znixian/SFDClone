/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package mapeditor;

import TWLSlick.BasicTWLGameState;
import TWLSlick.RootPane;
import de.matthiasmann.twl.Button;
import de.matthiasmann.twl.DialogLayout;
import de.matthiasmann.twl.DialogLayout.Group;
import de.matthiasmann.twl.Event;
import de.matthiasmann.twl.Label;
import de.matthiasmann.twl.SplitPane;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import sfdclone.Asset;

/**
 *
 * @author Campbell Suter
 */
public class MapEditorMainState extends BasicTWLGameState {

    private DialogLayout statusBar;
    private MapEditorWidget mapEditor;
    private SplitPane sidebar;
    private SplitPane mainSplit;
    private GridLayout toolkit;

    private final MapEditor parent;

    private final ObjectListItem.DragListener listener = new ObjectListItem.DragListener() {

        @Override
        public void dragStarted(ObjectListItem slot, Event evt) {
            // don't care
        }

        @Override
        public void dragging(ObjectListItem slot, Event evt) {
            // don't care
        }

        @Override
        public void dragStopped(ObjectListItem slot, Event evt) {
            int x = evt.getMouseX();
            int y = evt.getMouseY();
            if (!mapEditor.isInside(x, y)) {
                return;
            }
            x -= slot.getInnerWidth() / 2;
            y -= slot.getInnerHeight() / 2;
            EditorObject editorObject = new EditorObject(slot.getType());
            editorObject.getPosition().x = x - mapEditor.getX();
            editorObject.getPosition().y = y - mapEditor.getY();
            editorObject.getPosition().divide(mapEditor.getZoom());
            mapEditor.snapToGrid(editorObject.getPosition(), true);
            mapEditor.getObjs().add(editorObject);
            mapEditor.setSelected(editorObject);
        }
    };

    public MapEditorMainState(MapEditor parent) {
        this.parent = parent;
    }

    @Override
    protected RootPane createRootPane() {
        RootPane rp = super.createRootPane();
        rp.setTheme("");
        createMainSplit(rp);
        createToolkit();
        createMapEditor();
        createStatusBar(rp);
        return rp;
    }

    private void createMainSplit(RootPane rp) {
        mainSplit = new SplitPane();
        mainSplit.setDirection(SplitPane.Direction.HORIZONTAL);
        mainSplit.setSplitPosition(200);
        mainSplit.setRespectMinSizes(true);
        rp.add(mainSplit);
    }

    private void createToolkit() {
        Asset.initNoExec();
        toolkit = new GridLayout();
        ObjectListItem item = new ObjectListItem(this, new EditorObjectType(Asset.getBox1()));
        ObjectListItem item2 = new ObjectListItem(this, new EditorObjectType(Asset.getFloor1()));
        item.setListener(listener);
        item2.setListener(listener);
        toolkit.add(item);
        toolkit.add(item2);

        sidebar = new SplitPane();
        Button b;
        sidebar.add(b = new Button("Hi"));
        sidebar.add(toolkit);
        sidebar.setDirection(SplitPane.Direction.VERTICAL);
        sidebar.setSplitPosition(SplitPane.CENTER);
        sidebar.setRespectMinSizes(true);
        mainSplit.add(sidebar);

        b.adjustSize();
    }

    private void createMapEditor() {
        mapEditor = new MapEditorWidget(this);
        mainSplit.add(mapEditor);
    }

    private void createStatusBar(RootPane rp) {
        statusBar = new DialogLayout();
        statusBar.setTheme("statusbar");

        Button button = new Button("Click ME");
        button.addCallback(() -> {
            System.out.println("some action!");
        });

        Label label = new Label();
        label.setText("Info 1");

        Group hz = statusBar.createParallelGroup(statusBar
                .createSequentialGroup().addMinGap(DialogLayout.DEFAULT_GAP)
                .addWidget(label));
        Group vz = statusBar.createParallelGroup(statusBar
                .createSequentialGroup().addMinGap(DialogLayout.DEFAULT_GAP)
                .addWidget(button));

        Group horizontal = statusBar.createSequentialGroup(hz, vz);

        Group hz2 = statusBar.createSequentialGroup(label);
        Group vz2 = statusBar.createSequentialGroup(button);

        Group vertical = statusBar.createParallelGroup(hz2, vz2);

        statusBar.setHorizontalGroup(horizontal);
        statusBar.setVerticalGroup(vertical);

        rp.add(statusBar);
    }

    @Override
    protected void layoutRootPane() {
        int width = parent.getContainer().getWidth();
        int height = parent.getContainer().getHeight();

        mainSplit.setPosition(0, 30);
        mainSplit.setSize(width, height - 30);

        statusBar.setPosition(0, 0);
        statusBar.setSize(width, 30);
    }

    @Override
    public void init(GameContainer arg0, StateBasedGame arg1)
            throws SlickException {
    }

    @Override
    public void render(GameContainer arg0, StateBasedGame arg1, Graphics arg2)
            throws SlickException {
    }

    @Override
    public void update(GameContainer arg0, StateBasedGame arg1, int arg2)
            throws SlickException {
    }

    public MapEditor getParent() {
        return parent;
    }

    @Override
    public int getID() {
        return 0;
    }

    public MapEditorWidget getMapEditor() {
        return mapEditor;
    }
}
