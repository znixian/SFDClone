/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sfdclone;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

/**
 *
 * @author znix
 */
public class Asset {

    private Asset() {
    }

    private static boolean inited;

    public static boolean isInited() {
        return inited;
    }

    public static void initNoExec() {
        if (isInited()) {
            return;
        }
        try {
            init();
        } catch (SlickException ex) {
            Logger.getLogger(Asset.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void init() throws SlickException {
        if (inited) {
            throw new IllegalStateException("Assets have already been initialized!");
        }
        inited = true;

        Image sheet1_img = new Image("assets/image/sheet1.png");
        sheet1_img.setFilter(Image.FILTER_NEAREST);
        SpriteSheet sheet1 = new SpriteSheet(sheet1_img, 16, 16);
        // col 0
        floor1 = sheet1.getSubImage(0, 0);
        box1 = sheet1.getSubImage(1, 0);

        player = new Animation(new Image[]{
            new Image("assets/image/player1.png"),
            new Image("assets/image/player2.png"),
            new Image("assets/image/player3.png")
        }, 100);
    }

    //
    private static Image floor1;

    private static Image box1;

    private static Animation player;

    //
    public static Image getFloor1() {
        return floor1;
    }

    public static Image getBox1() {
        return box1;
    }

    public static Animation getPlayer() {
        return player;
    }
}
