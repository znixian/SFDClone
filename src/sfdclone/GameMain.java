/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sfdclone;

import java.util.Stack;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import sfdclone.states.LoadingState;

/**
 *
 * @author znix
 */
public class GameMain extends BasicGame {

    private Stack<State> states = new Stack<>();
    private GameContainer container;
    private int controllerID;

    public GameMain(String simple_Slick_Game) {
        super("SFD Clone");
    }

    @Override
    public void init(GameContainer container) throws SlickException {
        this.container = container;
        states = new Stack<>();
        addState(new LoadingState(this), container);
    }
    
    public void addState(State state, GameContainer container) throws SlickException {
        states.add(state);
        state.init(container);
    }
    
    public void removeState() throws SlickException {
        states.pop();
    }

    public BasicGame getCurrentState() {
        return states.peek();
    }

    public int getControllerID() {
        return controllerID;
    }

    public void setControllerID(int controllerID) {
        this.controllerID = controllerID;
    }

    public GameContainer getContainer() {
        return container;
    }

    // delgates

    @Override
    public void update(GameContainer container, int delta) throws SlickException {
        getCurrentState().update(container, delta);
    }

    @Override
    public void render(GameContainer container, Graphics g) throws SlickException {
        getCurrentState().render(container, g);
    }

    @Override
    public void keyPressed(int key, char c) {
        getCurrentState().keyPressed(key, c);
    }

    @Override
    public void keyReleased(int key, char c) {
        getCurrentState().keyReleased(key, c);
    }

    @Override
    public void mouseMoved(int oldx, int oldy, int newx, int newy) {
        getCurrentState().mouseMoved(oldx, oldy, newx, newy);
    }

    @Override
    public void mouseDragged(int oldx, int oldy, int newx, int newy) {
        getCurrentState().mouseDragged(oldx, oldy, newx, newy);
    }

    @Override
    public void mouseClicked(int button, int x, int y, int clickCount) {
        getCurrentState().mouseClicked(button, x, y, clickCount);
    }

    @Override
    public void mousePressed(int button, int x, int y) {
        getCurrentState().mousePressed(button, x, y);
    }

    @Override
    public void controllerButtonPressed(int controller, int button) {
        getCurrentState().controllerButtonPressed(controller, button);
    }

    @Override
    public void controllerButtonReleased(int controller, int button) {
        getCurrentState().controllerButtonReleased(controller, button);
    }

    @Override
    public void controllerDownPressed(int controller) {
        getCurrentState().controllerDownPressed(controller);
    }

    @Override
    public void controllerDownReleased(int controller) {
        getCurrentState().controllerDownReleased(controller);
    }

    @Override
    public void controllerLeftPressed(int controller) {
        getCurrentState().controllerLeftPressed(controller);
    }

    @Override
    public void controllerLeftReleased(int controller) {
        getCurrentState().controllerLeftReleased(controller);
    }

    @Override
    public void controllerRightPressed(int controller) {
        getCurrentState().controllerRightPressed(controller);
    }

    @Override
    public void controllerRightReleased(int controller) {
        getCurrentState().controllerRightReleased(controller);
    }

    @Override
    public void controllerUpPressed(int controller) {
        getCurrentState().controllerUpPressed(controller);
    }

    @Override
    public void controllerUpReleased(int controller) {
        getCurrentState().controllerUpReleased(controller);
    }

    @Override
    public void mouseReleased(int button, int x, int y) {
        getCurrentState().mouseReleased(button, x, y);
    }

    @Override
    public void mouseWheelMoved(int change) {
        getCurrentState().mouseWheelMoved(change);
    }
    
}
