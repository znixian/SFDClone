/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sfdclone.states;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import sfdclone.Asset;
import sfdclone.GameMain;
import sfdclone.State;

/**
 *
 * @author znix
 */
public class LoadingState extends State {

    public LoadingState(GameMain parent) {
        super(parent);
    }

    @Override
    public void init(GameContainer container) throws SlickException {
    }

    @Override
    public void update(GameContainer container, int delta) throws SlickException {
        Asset.init();

        parent.removeState();
        parent.addState(new IngameState(parent), container);
    }

    @Override
    public void render(GameContainer container, Graphics g) throws SlickException {
        g.setBackground(Color.black);
        g.clear();

        g.setColor(Color.white);
        String loadingText = "Loading...";

        int y = container.getHeight() / 2 - g.getFont().getLineHeight() / 2;
        int x = container.getWidth() / 2 - g.getFont().getWidth(loadingText) / 2;
        g.drawString(loadingText, x, y);
    }

}
