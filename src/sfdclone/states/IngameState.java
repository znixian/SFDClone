/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sfdclone.states;

import java.util.Collection;
import org.jbox2d.common.Vec2;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import sfdclone.GameMain;
import sfdclone.State;
import sfdclone.Vec2I;
import sfdclone.net.Action;
import sfdclone.net.Client;
import sfdclone.net.IEnviroment;
import sfdclone.net.Server;
import sfdclone.object.Player;
import sfdclone.object.WObject;
import static sfdclone.object.WObject.coordWorldToPixels;

/**
 *
 * @author znix
 */
public class IngameState extends State {

    private IEnviroment env;

    private final boolean[] keysLast = new boolean[0xFF];
    private boolean shootingLast;

    public IngameState(GameMain parent) {
        super(parent);
    }

    @Override
    public void init(GameContainer container) throws SlickException {
        env = Client.connect(this);
        if (env == null) {
            System.out.println("starting server...");
            env = new Server(true);
        }
        container.setMouseGrabbed(true);
    }

    @Override
    public void update(GameContainer container, int delta) throws SlickException {
        if (keysLast[Input.KEY_A] != keys[Input.KEY_A]) {
            env.setAction(Action.LEFT, keys[Input.KEY_A]);
            keysLast[Input.KEY_A] = keys[Input.KEY_A];
        }
        if (keysLast[Input.KEY_D] != keys[Input.KEY_D]) {
            env.setAction(Action.RIGHT, keys[Input.KEY_D]);
            keysLast[Input.KEY_D] = keys[Input.KEY_D];
        }
        if (keysLast[Input.KEY_W] != keys[Input.KEY_W]) {
            keysLast[Input.KEY_W] = keys[Input.KEY_W];
            if (keys[Input.KEY_W]) {
                env.triggerAction(Action.JUMP);
            }
        }
        if (shootingLast != buttons[Input.MOUSE_LEFT_BUTTON]) {
            shootingLast = buttons[Input.MOUSE_LEFT_BUTTON];
            if (shootingLast) {
                env.triggerAction(Action.SHOOT);
            }
        }
    }

    @Override
    public void render(GameContainer container, Graphics g) throws SlickException {
        g.setBackground(Color.blue);
        g.clear();

        Vec2I levelSize = env.getLevelSize();
        float widthRatio = 1f * container.getWidth() / levelSize.x;
        float heightRatio = 1f * container.getHeight() / levelSize.y;
        float scale = Math.min(widthRatio, heightRatio);
        g.pushTransform();
        g.scale(scale, scale);

        g.setColor(Color.black);
        g.drawRect(0, 0, levelSize.x, levelSize.y);

        Collection<WObject> objs = env.getObjs();
        synchronized (env.getObjsSync()) {
            objs.stream().forEach((obj) -> {
                obj.draw(g);
            });
        }
        drawAimingCircle(g, env.getPlayer());

        g.popTransform();
    }

    private void drawAimingCircle(Graphics g, Player player) {
        if (player == null) {
            return;
        }
        double aimAngle = player.getAimAngle();
        boolean flipped = aimAngle >= Math.PI;
        if (!buttons[Input.MOUSE_RIGHT_BUTTON]) {
            aimAngle = flipped ? Math.PI : 0;
            player.setAimAngle(aimAngle);
            return;
        }
        if (aimAngle == 0 || aimAngle == Math.PI) {
            aimAngle += Math.PI * 0.5;
        }
        Vec2 position = player.getData().getPosition();
        Vec2I pos = coordWorldToPixels(position.x, position.y, env.getLevelSize());
        int diam = 80;
        int radius = diam / 2;
        int laserDist = WObject.SIZE * 64;

        Input in = parent.getContainer().getInput();
        if (in.getControllerCount() > 0) {
            int controller = parent.getControllerID();
            float xAxis = in.getAxisValue(controller, 0);
            float yAxis = in.getAxisValue(controller, 0);
            aimAngle = Math.atan2(xAxis, yAxis);
        }

        player.setAimAngle(aimAngle);

        int crossHairSize = 6;
        int aimX = (int) (Math.sin(aimAngle) * radius) - crossHairSize / 2;
        int aimY = (int) (Math.cos(aimAngle) * radius) - crossHairSize / 2;

        int aimLaserX = (int) (Math.sin(aimAngle) * laserDist);
        int aimLaserY = (int) (Math.cos(aimAngle) * laserDist);

        g.setColor(Color.black);
        if (flipped) {
            g.drawArc(pos.x - radius, pos.y - radius, diam, diam, 90, 270);
        } else {
            g.drawArc(pos.x - radius, pos.y - radius, diam, diam, 270, 90);
        }
        g.drawRect(pos.x + aimX, pos.y + aimY, crossHairSize, crossHairSize);

        g.setColor(Color.red);
        g.drawLine(pos.x, pos.y, aimLaserX + pos.x, aimLaserY + pos.y);
    }

    @Override
    public void mouseMoved(int oldx, int oldy, int newx, int newy) {
        if (!buttons[Input.MOUSE_RIGHT_BUTTON]) {
            return;
        }
        double aimAngle = env.getPlayer().getAimAngle();
        boolean flipped = aimAngle > Math.PI;
        if (Math.abs(newx) > Math.abs(newy) * 5 && Math.abs(newx) > 15) {
            if (flipped && newx > 0) {
                aimAngle = 2 * Math.PI - aimAngle;
            } else if (!flipped && newx < 0) {
                aimAngle = Math.PI * 2 - aimAngle;
            }
        } else if (!flipped) {
            aimAngle -= newy * Math.PI / parent.getContainer().getHeight();
            aimAngle = Math.max(aimAngle, Math.PI * 0.001);
            aimAngle = Math.min(aimAngle, Math.PI * 0.999);
        } else {
            aimAngle += newy * Math.PI / parent.getContainer().getHeight();
            aimAngle = Math.max(aimAngle, Math.PI * 1.001);
            aimAngle = Math.min(aimAngle, Math.PI * 2);
        }
        env.getPlayer().setAimAngle(aimAngle);
    }

    @Override
    public void mouseDragged(int oldx, int oldy, int newx, int newy) {
        mouseMoved(oldx, oldy, newx, newy);
    }

}
