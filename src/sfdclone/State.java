/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sfdclone;

import org.newdawn.slick.BasicGame;

/**
 *
 * @author znix
 */
public abstract class State extends BasicGame {

    protected final GameMain parent;
    protected boolean[] keys = new boolean[0xFF];
    protected boolean[] buttons = new boolean[0xF];

    public State(GameMain parent) {
        super(null);
        this.parent = parent;
    }

    @Override
    public void keyPressed(int key, char c) {
        keys[key] = true;
    }

    @Override
    public void keyReleased(int key, char c) {
        keys[key] = false;
    }

    @Override
    public void mousePressed(int button, int x, int y) {
        buttons[button] = true;
    }

    @Override
    public void mouseReleased(int button, int x, int y) {
        buttons[button] = false;
    }

}
