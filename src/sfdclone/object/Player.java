/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sfdclone.object;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;
import sfdclone.net.MovementData;
import org.jbox2d.callbacks.RayCastCallback;
import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.Fixture;
import org.jbox2d.dynamics.FixtureDef;
import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import sfdclone.Asset;
import sfdclone.Vec2I;
import sfdclone.net.Action;
import sfdclone.net.ByteEncoder;
import sfdclone.net.C2SMessages;
import sfdclone.net.IDataSource;
import sfdclone.net.IEnviroment;
import sfdclone.net.PhysicsDataSource;
import sfdclone.net.Server;
import static sfdclone.object.WObject.coordWorldToPixels;
import sfdclone.physics.B2DWrapper;

/**
 *
 * @author znix
 */
public class Player extends WObject {

    private final MovementData movementData = new MovementData();
    private int health = 100;
    private double aimAngle;
    private boolean isOurClient;
    private boolean shouldC2SUpdate;
    private PlayerAnimationState animationState = PlayerAnimationState.STANDING;
    private final Animation animation = Asset.getPlayer().copy();

    public Player(B2DWrapper world, Server server, Vec2 pos) {
        super(world, server, pos);
    }

    public Player(IEnviroment state, IDataSource data) {
        super(state, data);
    }

    @Override
    public Body makeBody(B2DWrapper world, Vec2 pos) {

//        PolygonShape sd = new PolygonShape();
        CircleShape sd = new CircleShape();
        float box2dW = world.scalarPixelsToWorld(16 / 2);
        float box2dH = world.scalarPixelsToWorld(16 / 2);
//        sd.setAsBox(box2dW, box2dH);
        sd.m_radius = box2dH;

        // Define a fixture
        FixtureDef fd = new FixtureDef();
        fd.shape = sd;
        // Parameters that affect physics
        fd.density = (float) 0.0001;
        fd.friction = (float) 0.3;
        fd.restitution = (float) 0.0;
        fd.friction = (float) 0.0;

        fd.filter.categoryBits = FILTER_ENTITIES;
        fd.filter.maskBits = FILTERSET_SOLID;

        // Define the body and make it from the shape
        BodyDef bd = new BodyDef();
        bd.type = BodyType.DYNAMIC;
        bd.fixedRotation = true;
        bd.allowSleep = false; // keep the player alive.
        bd.linearDamping = 1;
        bd.position.set(world.coordPixelsToWorld(pos));

        Body body = world.createBody(bd);
        body.createFixture(fd);
        return body;
    }

    @Override
    public void serverUpdate(int delta, List<Consumer<List<WObject>>> delayedRuns) {
        if (health <= 0) {
            return;
        }
        if (movementData.shooting) {
            movementData.shooting = false;
            shoot(((PhysicsDataSource) data).world, delayedRuns);
        }
        Body body = ((PhysicsDataSource) data).body;
        if (aimAngle != Math.PI && aimAngle != 0) {
            movementData.jumping = false;
            body.m_linearVelocity.x = 0;
            animationState = PlayerAnimationState.AIMING;
            return;
        }
        float mass = body.getMass();
        float movementForce = 10 * delta * mass;
        float vertImpulse = 10 * mass;
        boolean strafe = false;
        animationState = PlayerAnimationState.STANDING;
        if (movementData.movingRight) {
            body.applyForce(new Vec2(movementForce, 0), body.getPosition());
            strafe = true;
        }
        if (movementData.movingLeft) {
            body.applyForce(new Vec2(-movementForce, 0), body.getPosition());
            strafe = true;
        }
        if (!strafe) {
            body.m_linearVelocity.x = 0;
        } else {
            animationState = PlayerAnimationState.RUNNING;
        }
        boolean canJump = body.m_contactList != null && Math.abs(body.m_linearVelocity.y) < 0.1;
        if (movementData.jumping) {
            if (canJump) {
                movementData.jumping = false;
                body.applyLinearImpulse(new Vec2(0, vertImpulse), body.getPosition());
            }
        }
        if (!canJump) {
            animationState = PlayerAnimationState.JUMPING;
        }
        Vec2 velo = body.m_linearVelocity;
        velo.x = Math.min(5, velo.x);
        velo.x = Math.max(-5, velo.x);
    }

    public MovementData getMovementData() {
        return movementData;
    }

    @Override
    protected Image getSprite() {
        return animation.getCurrentFrame();
    }

    public void shoot(B2DWrapper world, List<Consumer<List<WObject>>> delayedRuns) {
        double range = WObject.SIZE * 64;
        double angle = aimAngle;
        if (angle == 0 || angle == Math.PI) {
            angle += Math.PI * 0.5;
        }
        float aimX = (float) (Math.sin(angle) * range);
        float aimY = (float) (Math.cos(angle) * range);
        Vec2 end = world.vectorPixelsToWorld(aimX, aimY);
        BulletRaycaster ray = new BulletRaycaster();
        world.world.raycast(ray, data.getPosition(), data.getPosition().add(end));
        if (ray.collision != null) {
            WObject hit = ray.collision;
            if (hit instanceof Player) {
                Player player = ((Player) hit);
                player.reduceHealth(20);
            }
            if (hit instanceof PhysicsSprite) {
                delayedRuns.add((objs) -> {
                    world.destroyBody(((PhysicsDataSource) hit.getData()).body);
                    objs.remove(hit);
                });
            }
        }
    }

    @Override
    public void write(DataOutputStream out) throws IOException {
        out.write(10); // 1 for health + 1 for animation + 8 for aim angle

        out.write(health);
        out.write(animationState.ordinal());
        out.write(ByteEncoder.toByteArray(aimAngle));
    }

    @Override
    public void read(float x, float y, float rot, byte[] misc, int miscLength) {
        super.read(x, y, rot, misc, miscLength);
        health = misc[0];
        animationState = PlayerAnimationState.values()[misc[1]];
        if (!isOurClient) {
            aimAngle = ByteEncoder.toDouble(misc, 2);
        }
    }

    @Override
    public void draw(Graphics g) {
        super.draw(g);
        switch (animationState) {
            case JUMPING:
            case STANDING:
            case AIMING:
                animation.stop();
                animation.setCurrentFrame(0);
                break;
            case RUNNING:
            case WALKING:
                animation.start();
                break;
        }
        animation.updateNoDraw();
        Vec2 position = data.getPosition();
        Vec2I pos = coordWorldToPixels(position.x, position.y, state.getLevelSize());
        Image sprite = getSprite();
        int width = sprite.getWidth() * 2;
        int healthwidth = sprite.getWidth() * 2 * health / 100;
        int height = 8;

        g.setColor(Color.black);
        g.fillRect(pos.x - width / 2, pos.y - sprite.getHeight() - height, width, height);
        g.setColor(Color.red);
        g.fillRect(pos.x - width / 2, pos.y - sprite.getHeight() - height, healthwidth, height);
    }

    public void C2SUpdate(DataOutputStream out) throws IOException {
        if (!shouldC2SUpdate) {
            return;
        }
        shouldC2SUpdate = false;

        out.write(C2SMessages.PLAYER_UPDATE.ordinal());
        out.writeDouble(aimAngle);
    }

    public double getAimAngle() {
        return aimAngle;
    }

    public void setAimAngle(double aimAngle) {
        if (aimAngle != this.aimAngle) {
            shouldC2SUpdate = isOurClient;
        }
        this.aimAngle = aimAngle;
    }

    public boolean isIsOurClient() {
        return isOurClient;
    }

    public void setIsOurClient(boolean isOurClient) {
        this.isOurClient = isOurClient;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
        if (health <= 0) {
            this.health = 0;
        }
    }

    public void reduceHealth(int health) {
        setHealth(getHealth() - health);
    }

    private static enum PlayerAnimationState {

        RUNNING, WALKING, STANDING, COVERING, AIMING, JUMPING;
    }

    public class BulletRaycaster implements RayCastCallback {

        public WObject collision;
        public float minFrac = Float.MAX_VALUE;

        @Override
        public float reportFixture(Fixture fixture, Vec2 point, Vec2 normal, float fraction) {
//            fixture.destroy();
            Object ud = fixture.getBody().getUserData();
            if (minFrac > fraction && (ud instanceof WObject)) {
                minFrac = fraction;
                collision = (WObject) ud;
//                return 0;
            }
            return -1;
        }

    }
}
