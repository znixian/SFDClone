/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sfdclone.object;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;
import sfdclone.net.PhysicsDataSource;
import sfdclone.net.IDataSource;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import sfdclone.ExtMath;
import sfdclone.Vec2I;
import sfdclone.net.IEnviroment;
import sfdclone.net.Server;
import sfdclone.net.StoredDataSource;
import sfdclone.physics.B2DWrapper;

/**
 * World Object - any object in the world that interfears with physics.
 * @author znix
 */
public abstract class WObject {

    public static final int SIZE = 16;

    public static final int FILTER_NONE = 0x0000;
    public static final int FILTER_WALLS = 0x0002;
    public static final int FILTER_ENTITIES = 0x0004;
    public static final int FILTER_SOLID_ENTITIES = 0x0008;
    public static final int FILTERSET_SOLID = FILTER_WALLS | FILTER_SOLID_ENTITIES;
    public static final int FILTERSET_ALL = 0xFFFF;

    protected final IDataSource data;
    protected IEnviroment state;

    public WObject(B2DWrapper wrapper, Server server, Vec2 pos) {
        this.state = server;
        data = new PhysicsDataSource(this, wrapper, pos);
    }

    public WObject(IEnviroment state, IDataSource data) {
        this.state = state;
        this.data = data;
    }

    protected abstract Image getSprite();

    public abstract Body makeBody(B2DWrapper world, Vec2 pos);

    public void draw(Graphics g) {
        Vec2 position = data.getPosition();
        Vec2I pos = coordWorldToPixels(position.x, position.y, state.getLevelSize());

        Image sprite = getSprite();

        g.pushTransform();
        g.setColor(Color.white);
        g.rotate(pos.x, pos.y, (float) (-data.getAngle() * 180 / Math.PI));
        int x = pos.x - sprite.getWidth() / 2;
        int y = pos.y - sprite.getHeight() / 2;
        g.drawImage(sprite,
                x, y, // x + 16, y + 16,
                0, 0, 16, 16);
        g.popTransform();
    }

    public void serverUpdate(int delta, List<Consumer<List<WObject>>> delayedRuns) {
    }

    public static Vec2I coordWorldToPixels(float worldX, float worldY, Vec2I size) {
        float pixelX = ExtMath.map(worldX, 0f, 1f, size.x / 2f, size.y / 2f + 10);
        float pixelY = ExtMath.map(worldY, 0f, 1f, size.x / 2f, size.y / 2f + 10);
        pixelY = ExtMath.map(pixelY, 0f, size.y, size.y, 0f);
        return new Vec2I(pixelX, pixelY);
    }

    public IDataSource getData() {
        return data;
    }

    public void read(float x, float y, float rot, byte[] misc, int miscLength) {
        StoredDataSource data
                = (StoredDataSource) getData();
        data.getPosition().x = x;
        data.getPosition().y = y;
        data.setAngle(rot);
    }

    public void write(DataOutputStream out) throws IOException {
        out.write(0);
    }
}
