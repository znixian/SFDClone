/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sfdclone.object;

import sfdclone.net.IEnviroment;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.newdawn.slick.Image;
import sfdclone.Asset;
import sfdclone.net.IDataSource;
import sfdclone.net.Server;
import sfdclone.physics.B2DWrapper;

/**
 *
 * @author znix
 */
public class SolidSprite extends WObject {

    public SolidSprite(B2DWrapper world, Server server, Vec2 pos) {
        super(world, server, pos);
    }

    public SolidSprite(IEnviroment state, IDataSource data) {
        super(state, data);
    }

    @Override
    public Body makeBody(B2DWrapper world, Vec2 pos) {

        PolygonShape sd = new PolygonShape();
        float box2dW = world.scalarPixelsToWorld(getSprite().getWidth() / 2);
        float box2dH = world.scalarPixelsToWorld(getSprite().getHeight() / 2);
        sd.setAsBox(box2dW, box2dH);

        // Define a fixture
        FixtureDef fd = new FixtureDef();
        fd.shape = sd;
        // Parameters that affect physics
        fd.density = 1;
        fd.friction = (float) 0.3;
        fd.restitution = (float) 0; // 0.5;
        
        fd.filter.categoryBits = FILTER_WALLS;

        // Define the body and make it from the shape
        BodyDef bd = new BodyDef();
        bd.type = BodyType.STATIC;
        bd.position.set(world.coordPixelsToWorld(pos));

        Body body = world.createBody(bd);
        body.createFixture(fd);
        return body;
    }

    @Override
    protected Image getSprite() {
        return Asset.getFloor1();
    }

}
