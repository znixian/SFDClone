/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sfdclone.maploader;

import java.util.Collection;
import org.jbox2d.common.Vec2;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.Layer;
import sfdclone.net.Server;
import sfdclone.object.PhysicsSprite;
import sfdclone.object.SolidSprite;
import sfdclone.object.WObject;
import sfdclone.physics.B2DWrapper;

/**
 *
 * @author znix
 */
public class Map {

    private final HSTiledMap map;

    public Map() throws SlickException {
        map = new HSTiledMap("assets/levels/map1.tmx", false);
    }

    public void initServer(Server server) {
        Collection<WObject> objs = server.getObjs();
        B2DWrapper wrapper = server.getWrapper();

        Layer layer = map.getLayer(0);

        int offsetX = (int) (WObject.SIZE * 1.5);
        int offsetY = (int) (WObject.SIZE * 1.5);
        
        for (int x = 0; x < layer.width; x++) {
            for (int y = 0; y < layer.height; y++) {
                int id = layer.data[x][y][2];
                if (id == 0) {
                    continue;
                }
                Vec2 pos = new Vec2(x * WObject.SIZE + offsetX, y * WObject.SIZE + offsetY);
                switch (id) {
                    case 1:
                        objs.add(new SolidSprite(wrapper, server, pos));
                        break;
                    case 2:
                        objs.add(new PhysicsSprite(wrapper, server, pos));
                        break;
                    default:
                        throw new RuntimeException("Illegal ID " + id + " at (" + x + "," + y + ")");
                }
            }
        }
    }
}
