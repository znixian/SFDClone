/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sfdclone;

import org.jbox2d.common.Vec2;

/**
 *
 * @author znix
 */
public class Vec2I {

    public int x, y;

    public Vec2I() {
    }

    public Vec2I(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Vec2I(float x, float y) {
        this.x = Math.round(x);
        this.y = Math.round(y);
    }

    @Override
    public String toString() {
        return "Vec2I{" + "x=" + x + ", y=" + y + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + this.x;
        hash = 17 * hash + this.y;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vec2I other = (Vec2I) obj;
        if (this.x != other.x) {
            return false;
        }
        if (this.y != other.y) {
            return false;
        }
        return true;
    }

    public Vec2 toVec2() {
        return new Vec2(x, y);
    }

    public void set(Vec2I vec) {
        x = vec.x;
        y = vec.y;
    }

    public void mult(float factor) {
        x *= factor;
        y *= factor;
    }

    public void divide(float factor) {
        x /= factor;
        y /= factor;
    }

    public void add(int xx, int yy) {
        x += xx;
        y += yy;
    }
}
