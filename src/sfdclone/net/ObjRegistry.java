/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sfdclone.net;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import sfdclone.object.PhysicsSprite;
import sfdclone.object.Player;
import sfdclone.object.SolidSprite;
import sfdclone.object.WObject;

/**
 *
 * @author znix
 */
public final class ObjRegistry {

    private int nextID;
    private final Map<Class<? extends WObject>, Integer> objects;
    private final Map<Integer, Function<IEnviroment, WObject>> constructors;
    private final Object sync;

    public void registerObject(Class<? extends WObject> obj, Function<IEnviroment, WObject> constructor) {
        synchronized (sync) {
            int id = nextID++;
            objects.put(obj, id);
            constructors.put(id, constructor);
        }
    }

    public int getID(Class<? extends WObject> obj) {
        synchronized (sync) {
            return objects.get(obj);
        }
    }

    public WObject construct(int id, IEnviroment state) {
        synchronized (sync) {
            return constructors.get(id).apply(state);
        }
    }

    private ObjRegistry() {
        objects = new HashMap<>();
        constructors = new HashMap<>();
        sync = new Object();

        // register stuff
        registerObject(PhysicsSprite.class, (state) -> {
            return new PhysicsSprite(state, new StoredDataSource());
        });
        registerObject(SolidSprite.class, (state) -> {
            return new SolidSprite(state, new StoredDataSource());
        });
        registerObject(Player.class, (state) -> {
            return new Player(state, new StoredDataSource());
        });
    }

    // singleton
    public static ObjRegistry getInstance() {
        return ObjRegistryHolder.INSTANCE;
    }

    private static class ObjRegistryHolder {

        private static final ObjRegistry INSTANCE = new ObjRegistry();
    }
}
