/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sfdclone.net;

import org.jbox2d.common.Vec2;

/**
 *
 * @author znix
 */
public class StoredDataSource implements IDataSource {

    private Vec2 position = new Vec2();
    private float angle;

    /**
     * Set the value of position
     * If this is {@code null}, then it is set to 0,0
     *
     * @param position new value of position
     */
    public void setPosition(Vec2 position) {
        this.position = position;
        if (position == null) {
            this.position = new Vec2();
        }
    }

    /**
     * Set the value of angle
     *
     * @param angle new value of angle
     */
    public void setAngle(float angle) {
        this.angle = angle;
    }

    /**
     * Get the value of position
     *
     * @return the value of position
     */
    @Override
    public Vec2 getPosition() {
        return position;
    }

    /**
     * Get the value of angle
     *
     * @return the value of angle
     */
    @Override
    public float getAngle() {
        return angle;
    }

}
