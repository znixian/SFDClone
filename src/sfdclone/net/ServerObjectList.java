/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sfdclone.net;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.LinkedList;
import sfdclone.object.WObject;

/**
 *
 * @author znix
 */
public class ServerObjectList extends LinkedList<WObject> {

    private final Server owner;

    public ServerObjectList(Server owner) {
        this.owner = owner;
    }

    @Override
    public WObject remove(int index) {
        return onObjectRemoved(super.remove(index));
    }

    @Override
    public boolean remove(Object o) {
        onObjectRemoved((WObject) o);
        return super.remove(o);
    }

    private WObject onObjectRemoved(WObject o) {
        PhysicsDataSource data = (PhysicsDataSource) o.getData();
        int id = data.id;
        NetAction act = (DataInputStream in, DataOutputStream out) -> {
            out.write(S2CMessage.ITEM_REMOVED.ordinal());
            out.writeInt(id);
        };
        owner.getPlayers().stream().forEach((player) -> {
            player.actions.add(act);
        });
        return o;
    }
}
