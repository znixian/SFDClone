/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sfdclone.net;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import sfdclone.object.WObject;
import sfdclone.physics.B2DWrapper;

/**
 *
 * @author znix
 */
public class PhysicsDataSource implements IDataSource {
    
    private static int nextID = 1; // the next unique ID to use.
    
    public final Body body;
    public final int id;
    public final B2DWrapper world;

    public PhysicsDataSource(WObject obj, B2DWrapper wrapper, Vec2 pos) {
        body = obj.makeBody(wrapper, pos);
        body.setUserData(obj);
        id = nextID++;
        world = wrapper;
    }

    @Override
    public Vec2 getPosition() {
        return body.getPosition();
    }

    @Override
    public float getAngle() {
        return body.getAngle();
    }
    
}
