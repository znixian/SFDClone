/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sfdclone.net;

import org.jbox2d.common.Vec2;
import sfdclone.object.Player;
import static sfdclone.object.WObject.SIZE;

/**
 *
 * @author znix
 */
public class HostClient {

    private final Server server;
    private final Player player;

    public HostClient(Server server) {
        this.server = server;
        synchronized (server.getWrapper()) {
            player = new Player(server.getWrapper(), server, new Vec2(50, (float) (SIZE * 3.5)));
        }
        server.getObjs().add(player);
    }

    public void setAction(Action action, boolean state) {
        MovementData data = player.getMovementData();
        switch(action) {
            case LEFT:
                data.movingLeft = state;
                break;
            case RIGHT:
                data.movingRight = state;
                break;
            case JUMP:
                data.jumping = state;
                break;
            case SHOOT:
                data.shooting = state;
                break;
        }
    }

    public void triggerAction(Action action) {
        setAction(action, true);
    }

    public Player getPlayer() {
        return player;
    }

}
