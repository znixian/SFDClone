/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sfdclone.net;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.logging.Level;
import java.util.logging.Logger;
import sfdclone.Vec2I;
import sfdclone.object.Player;
import sfdclone.object.WObject;
import sfdclone.states.IngameState;

/**
 *
 * @author znix
 */
public class Client implements IEnviroment {

    public static final int PORT = 8593;

    private final Socket socket;
    private final Map<Integer, WObject> objs;
    private Vec2I levelSize = new Vec2I();
    private final IngameState state;
    private final Queue<NetAction> actionQueue;
    private Player player;

    private Client(IngameState state) throws IOException {
        this.state = state;
        actionQueue = new LinkedList<>();
        objs = (Map<Integer, WObject>) Collections.synchronizedMap(new HashMap<Integer, WObject>());
        socket = new Socket("localhost", PORT);
        new Thread(() -> {
            try {
                System.out.println("Connected.");
                DataInputStream in = new DataInputStream(socket.getInputStream());
                DataOutputStream out = new DataOutputStream(
                        new BufferedOutputStream(socket.getOutputStream()));
                S2CMessage[] messages = S2CMessage.values();
                while (!socket.isConnected()) {
                    Thread.sleep(100);
                }

                {
                    out.write(C2SMessages.WORLD_INFO.ordinal());
                    out.flush();
                    int width = in.readInt();
                    int height = in.readInt();
                    levelSize.x = width;
                    levelSize.y = height;

                    int playerID = in.readInt();
                    player = new Player(this, new StoredDataSource());
                    player.setIsOurClient(true);
                    objs.put(playerID, player);
                }

                ArrayList<Integer> toQuery = new ArrayList<>();
                while (socket.isConnected() && !socket.isClosed()) {
                    out.write(C2SMessages.POLL.ordinal());
                    out.flush();
                    int numOps = in.readInt();
                    byte[] data = new byte[0xFF];
                    for (int i = 0; i < numOps; i++) {
                        S2CMessage msg = messages[in.read()];
                        switch (msg) {
                            case PHYSICS_UPDATE: {
                                float x = in.readFloat();
                                float y = in.readFloat();
                                float rot = in.readFloat();
                                int id = in.readInt();
                                int misc = in.read();
                                in.read(data, 0, misc);
                                WObject obj = objs.get(id);
                                if (obj == null) {
                                    toQuery.add(id);
                                } else {
                                    obj.read(x, y, rot, data, misc);
                                }
                                break;
                            }
                            case ITEM_REMOVED: {
                                int id = in.readInt();
                                objs.remove(id);
                                break;
                            }
                        }
                    }
                    for (Integer objID : toQuery) {
                        out.write(C2SMessages.PHYSICS_QUERY.ordinal());
                        out.writeInt(objID);
                        out.flush();
                        int typeID = in.readInt();
                        WObject obj = ObjRegistry.getInstance().construct(typeID, this);
                        objs.put(objID, obj);
                    }
                    while (!actionQueue.isEmpty()) {
                        NetAction action;
                        synchronized (actionQueue) {
                            action = actionQueue.remove();
                        }
                        action.accept(in, out);
                    }
                    toQuery.clear();
                    player.C2SUpdate(out);
                    // anything not flushed will be next time.
                }
                System.out.println("Disconnected.");
                System.exit(0);
            } catch (IOException | InterruptedException ex) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                System.exit(1);
            }
        }).start();
    }

    @Override
    public Collection<WObject> getObjs() {
        return objs.values();
    }

    @Override
    public Object getObjsSync() {
        return objs;
    }

    public static Client connect(IngameState state) {
        try {
            return new Client(state);
        } catch (ConnectException ex) {
            // do nothing
        } catch (IOException ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public Vec2I getLevelSize() {
        return levelSize;
    }

    @Override
    public Player getPlayer() {
        return player;
    }

    @Override
    public void setAction(Action action, boolean state) {
        synchronized (actionQueue) {
            actionQueue.add((in, out) -> {
                out.write(C2SMessages.ACTION_SEND.ordinal());
                out.write(action.ordinal());
                out.writeBoolean(state);
            });
        }
    }

    @Override
    public void triggerAction(Action action) {
        synchronized (actionQueue) {
            actionQueue.add((in, out) -> {
                out.write(C2SMessages.ACTION_SEND.ordinal());
                out.write(action.ordinal());
                out.writeBoolean(false);
            });
        }
    }
}
