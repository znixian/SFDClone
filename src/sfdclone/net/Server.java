/*
 * Copyright (C) 2016 Campbell Suter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package sfdclone.net;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jbox2d.common.Vec2;
import org.newdawn.slick.SlickException;
import sfdclone.Vec2I;
import sfdclone.maploader.Map;
import sfdclone.object.Player;
import sfdclone.object.WObject;
import static sfdclone.object.WObject.SIZE;
import sfdclone.physics.B2DWrapper;

/**
 *
 * @author znix
 */
public class Server implements IEnviroment {

    public ServerSocket socket;

    private B2DWrapper wrapper;
    private Vec2I levelSize;

    private List<WObject> objs;
    private List<ServerPlayerData> players;

    private HostClient localPlayer;

    public Server(boolean localPlayer) throws SlickException {
        players = Collections.synchronizedList(new ArrayList<>());
        try {
            socket = new ServerSocket(Client.PORT);
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        initWorld();
        if (localPlayer) {
            this.localPlayer = new HostClient(this);
        }
        new Thread(() -> {
            try {
                for (;;) {
                    Socket client = socket.accept();
                    new Thread(() -> {
                        handleClient(client);
                    }).start();
                }
            } catch (IOException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    socket.close();
                } catch (IOException ex) {
                    Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();

        // update thread
        new Thread(() -> {
            try {
                long now = System.currentTimeMillis();
                long last = now;
                List<Consumer<List<WObject>>> delayedRuns = new ArrayList<>();
                while (true) {
                    Thread.sleep(1000 / 60);
                    now = System.currentTimeMillis();
                    update((int) (now - last), delayedRuns);
                    last = now;
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }).start();
    }

    private void update(int delta, List<Consumer<List<WObject>>> delayedRuns) {
        synchronized (wrapper) {
            wrapper.step(delta / 1000f, 10, 8);
        }
        synchronized (objs) {
            objs.stream().forEach((obj) -> {
                obj.serverUpdate(delta, delayedRuns);
            });
            delayedRuns.stream().forEach((delayedRun) -> {
                delayedRun.accept(objs);
            });
            delayedRuns.clear();
        }
//        if (Math.random() < 1.0 / 1000 * delta) {
//            ArrayList<WObject> objs2 = new ArrayList<>();
//            for (WObject obj : objs) {
//                if (obj instanceof PhysicsSprite) {
//                    objs2.add(obj);
//                }
//            }
//            if (!objs2.isEmpty()) {
//                WObject obj = objs2.get((int) (Math.random() * objs2.size()));
//                ((PhysicsDataSource) obj.getData()).body.applyAngularImpulse(100);
//            }
//        }
    }

    private void initWorld() throws SlickException {
        levelSize = new Vec2I(500, 500);
        wrapper = new B2DWrapper(levelSize);
        wrapper.createWorld(new Vec2(0, -10));

        objs = Collections.synchronizedList(new ServerObjectList(this));
        Map map = new Map();
        map.initServer(this);
    }

    private void handleClient(Socket socket) {
        Player player;
        synchronized (wrapper) {
            player = new Player(wrapper, this, new Vec2(50, (float) (SIZE * 3.5)));
        }
        ServerPlayerData playerData = new ServerPlayerData(player);
        players.add(playerData);
        try {
            DataInputStream in = new DataInputStream(socket.getInputStream());
            DataOutputStream out = new DataOutputStream(
                    new BufferedOutputStream(socket.getOutputStream()));
            objs.add(player);
            C2SMessages[] messages = C2SMessages.values();
            while (socket.isConnected() && !socket.isClosed()) {
                C2SMessages msg = messages[in.read()];
                switch (msg) {
                    case POLL: {
                        out.writeInt(objs.size() + playerData.actions.size());
                        for (WObject obj : objs) {
                            out.write(S2CMessage.PHYSICS_UPDATE.ordinal());
                            PhysicsDataSource data
                                    = (PhysicsDataSource) obj.getData();
                            Vec2 pos = data.getPosition();
                            out.writeFloat(pos.x);
                            out.writeFloat(pos.y);
                            out.writeFloat(data.getAngle());
                            out.writeInt(data.id);
                            obj.write(out);
                        }
                        for (NetAction action : playerData.actions) {
                            action.accept(in, out);
                        }
                        playerData.actions.clear();
                        break;
                    }
                    case PHYSICS_QUERY: {
                        int id = in.readInt();
                        for (WObject obj : objs) {
                            PhysicsDataSource data
                                    = (PhysicsDataSource) obj.getData();
                            if (data.id == id) {
                                int tid = ObjRegistry.getInstance().getID(obj.getClass());
                                out.writeInt(tid);
                                break;
                            }
                        }
                        break;
                    }
                    case WORLD_INFO: {
                        out.writeInt(levelSize.x);
                        out.writeInt(levelSize.y);
                        out.writeInt(((PhysicsDataSource) player.getData()).id);
                        break;
                    }
                    case ACTION_SEND: {
                        Action action = Action.values()[in.read()];
                        boolean state = in.readBoolean();
                        switch (action) {
                            case LEFT:
                                player.getMovementData().movingLeft = state;
                                break;
                            case RIGHT:
                                player.getMovementData().movingRight = state;
                                break;
                            case JUMP:
                                player.getMovementData().jumping = true;
                                break;
                            case SHOOT:
                                player.getMovementData().shooting = true;
                                break;
                        }
                        break;
                    }
                    case PLAYER_UPDATE: {
                        player.setAimAngle(in.readDouble());
                    }
                }
                out.flush();
            }
            in.close();
            out.close();
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            synchronized (wrapper) {
                wrapper.destroyBody(((PhysicsDataSource) player.getData()).body);
                objs.remove(player);
            }
            try {
                socket.close();
            } catch (IOException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public B2DWrapper getWrapper() {
        return wrapper;
    }

    @Override
    public Collection<WObject> getObjs() {
        return objs;
    }

    @Override
    public Object getObjsSync() {
        return objs;
    }

    @Override
    public Vec2I getLevelSize() {
        return levelSize;
    }

    @Override
    public void setAction(Action action, boolean state) {
        localPlayer.setAction(action, state);
    }

    @Override
    public void triggerAction(Action action) {
        localPlayer.triggerAction(action);
    }

    @Override
    public Player getPlayer() {
        return localPlayer.getPlayer();
    }

    public List<ServerPlayerData> getPlayers() {
        return players;
    }
}
